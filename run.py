from utils import (
    list_from_csv, plot_scores
)
score_data = list_from_csv()
plot_scores(score_data)
