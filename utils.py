from data import SCORE_FILE
import csv
import datetime as dt
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

def get_datetime_from_date(date):
    return dt.datetime.strptime(date,'%Y-%m-%d')

def list_from_csv():

    f = open(SCORE_FILE,'r')
    reader = csv.reader(f)
    score_list = []
    for row in reader:
        score_list.append(row)
    score_data_list = []
    score_data_list.append(score_list[0])
    for row in score_list[1:]:
        score_data_list.append([row[0],int(row[1]),int(row[2])])
    return score_data_list

def plot_scores(score_data):
    player1 = score_data[0][1]
    player2 = score_data[0][2]
    dates = [get_datetime_from_date(row[0]) for row in score_data[1:]]

    score_p1 = [row[1] for row in score_data[1:]]
    score_p2 = [row[2] for row in score_data[1:]]
    """
    plt.xlabel('Date')
    plt.ylabel('Scores')
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y'))
    plt.gca().xaxis.set_major_locator(mdates.DayLocator())
    #plt.plot(dates,score_p1,'ks',dates,score_p2,'bs')
    plt.plot(dates,score_p1,'ks',label=player1)
    plt.plot(dates,score_p2,'bs',label=player2)
    plt.gcf().autofmt_xdate()
    #plt.show()
    plt.title("Match Stats")
    """
    fib, ax = plt.subplots()
    ax.plot(dates, score_p1, 'ks', label=player1)
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y'))
    plt.gca().xaxis.set_major_locator(mdates.DayLocator())
    plt.gcf().autofmt_xdate()
    ax.plot(dates, score_p2, 'bs', label=player2)
    
    #legend = ax.legend(loc='upper center', shadow=True)
    legend = ax.legend(loc='upper center')#, shadow=True)

    frame = legend.get_frame()
    #frame.set_facecolor('0.90')

    for label in legend.get_texts():
        label.set_fontsize('large')

    for label in legend.get_lines():
        label.set_linewidth(1.5)
    plt.savefig('tt.png')

